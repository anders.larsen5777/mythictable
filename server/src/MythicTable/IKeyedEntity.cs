﻿namespace MythicTable
{
    public interface IKeyedEntity
    {
        string Id { get; }
    }
}